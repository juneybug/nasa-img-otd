#!/usr/bin/env python

import urllib, datetime, os

import feedparser
from gi.repository.Gio import Settings

# Set the source feed.
feedURL = "http://www.nasa.gov/rss/lg_image_of_the_day.rss"

# Set the wallpaper folder.
home = os.path.expanduser("~")
wpFolder = home + "/Pictures/nasa-images/" 

def main(feedURL, wpFolder):
    # Parse the feed URL.
    feedInfo = feedparser.parse(feedURL)

    imageURL = feedInfo.entries[0].enclosures[0]["href"]

    # Extract the date of publishing.
    feedDate = feedInfo.entries[0].published[5:16]

    # Reformat the date for the picture archive.
    stripDate = datetime.datetime.strptime(feedDate, "%d %b %Y")
    fileDate = stripDate.strftime("%Y-%b-%d")

    localDownload = wpFolder + fileDate + ".jpg"

    # Download the picture to localDownload path.
    try:
        urllib.urlretrieve(imageURL, localDownload)
    except IOError:  # Make the directory if it does not exist.
        os.makedirs(wpFolder)

    # Create a new Settings object and set the picture-uri.
    bg_setting = Settings.new('org.cinnamon.desktop.background')
    bg_setting.set_string("picture-uri", "file://" + localDownload)
    bg_setting.set_string("picture-options", "zoom")
    bg_setting.apply()

if __name__ == '__main__':
    main(feedURL, wpFolder)
