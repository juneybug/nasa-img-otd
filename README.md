#NASA Image of the Day Wallpaper#

This file downloads the most recent large NASA Image of the Day and sets it as the wallpaper for the Cinnamon desktop, set to zoom.

Running it should be as simple as `./wallpaper-script.py`!

Future commits may include:

-adding scheduling functionality (e.g. adding cronjobs in linux)
-adding text over the image, such as through an image editor or through conky
